export interface Todo {
  id: number;
  task: string;
}

export interface Page<T> {
  total: number;
  limit: number;
  skip: number;
  data: T[];
}
export interface Offer {
  id?: number;
  title: string;
  location?: string;
  image?: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  userId: number;
}  

export interface User {
  id?: number;
  email: string;
  
}